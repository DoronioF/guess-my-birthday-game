import random

name = input("What is your name? ")

print(f"\nWelcome {name}!")

for x in range(3):
    year = random.randint(1900,2022)
    month = random.randint(1,12)
    response = input(f"\nIs this your birthday, {month}/{year}? Yes or No? " ).lower()
    if x < 2: 
        if(response == "yes"):
            print("\nI knew it!")
            exit()
        elif response == "no":
            print("\nDrat! Lemme try again!")
        else:
            print("\nI don't know how to process this.")
    else:
        print('\nOut of guesses!')
        exit()