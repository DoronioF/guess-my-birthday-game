import random

name = input('Hello! What is your name? ' )
print(f'Welcome {name}! I will guess your birthday in mm/yyyy format.')

guess_lim = 3
guess_ct = 0

while guess_ct < guess_lim:
    rand_month = random.randint(1, 12)
    rand_year = random.randint(1900, 2022)

    guess_ct += 1

    guess = input(f"Is this your birthday, {rand_month}/{rand_year}? Yes or No? " ).lower()
    
    if guess_ct < 3:
        if guess == 'yes':
            print('Yes, I knew it!')
            break
        elif guess == 'no':
            print('Drat! Lemme try again!')
    else:
        print('Out of guesses')
